﻿using Microsoft.Kinect;

namespace LightBuzz.Vitruvius.Gestures
{
    public class TurnLeftSegment : IGestureSegment
    {
        public GesturePartResult Update(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.HipCenter].Position.Y &&
                   skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.HipCenter].Position.Y)
            {
                return GesturePartResult.Succeeded;
            }

            return GesturePartResult.Failed;
        }
    }
}