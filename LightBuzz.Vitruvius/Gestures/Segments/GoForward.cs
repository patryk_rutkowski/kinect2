﻿using Microsoft.Kinect;

namespace LightBuzz.Vitruvius.Gestures
{
    public class GoForward : IGestureSegment
    {

        public GesturePartResult Update(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.HandRight].Position.Z < skeleton.Joints[JointType.ElbowRight].Position.Z - 0.2 &&
                skeleton.Joints[JointType.HandLeft].Position.Z < skeleton.Joints[JointType.ElbowLeft].Position.Z - 0.2)
            {
                if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.Head].Position.Y && skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.HipCenter].Position.Y &&
                    skeleton.Joints[JointType.HandLeft].Position.Y < skeleton.Joints[JointType.Head].Position.Y && skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.HipCenter].Position.Y)
                {
                    return GesturePartResult.Succeeded;
                }

            }

            return GesturePartResult.Failed;
        }
    }
}